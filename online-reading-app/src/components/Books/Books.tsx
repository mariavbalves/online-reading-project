import React from 'react';
// @ts-ignore
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import NavBar from "../navBar";

import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Button,
    Card,
    CardGroup,
    CardImg,
    Col,
    Container,
    Form,
    FormControl,
    Image,
    Nav,
    Navbar,
    Row
} from "react-bootstrap";
import Footer from "../footer";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebook, faInstagram, faTwitter, faYoutube} from "@fortawesome/free-brands-svg-icons";


export default class Books extends React.Component {
    render() {

        return <div>


            <Card >


                <NavBar/>

                <Card.Header>
                </Card.Header>


                <Card.Body>

                    <Card.Text>



                        <Container className="text-center">
                            <h1>Books</h1>


                        </Container>


                            <Card bg="light" style={{width: '100%'}} className='justify-content-lg-center'>




                                <Row>
                                    <Col xs={2} md={2}>
                                        <div style={{backgroundColor: "#3F78EA", padding:"10px"}} >
                                            <h4>Genres</h4>
                                            <div style={{margin: "10px"}} >
                                        <Link to="#">  <Form.Check id={"link"} style={{color:"#003d6b"}} type="checkbox" label="Action()" /> </Link>

                                        <Link to="#"><p id={"link"}> Fiction ()</p></Link>
                                        <Link to="#"><p id={"link"}> Romance () </p></Link>
                                        <Link to="#"><p id={"link"}> Thriller () </p></Link>
                                            <Link to="#"><p id={"link"}> Thriller () </p></Link>
                                            <Link to="#"><p id={"link"}> Classics () </p></Link>
                                            <Link to="#"><p id={"link"}> Fantasy () </p></Link>
                                            <Link to="#"><p id={"link"}> Others () </p></Link>
                                            <Link to="#"><p id={"link"}> All () </p></Link>
                                            </div>


                                            <h4>Trends</h4>
                                        </div>

                                    </Col>
                                    <Col xs={10} md={10} >
                                        <div style={{backgroundColor: "#8CABDF", borderRadius:'25px'}} >

                                            <Container style={{padding: "40px"}}>
                                                <Row>

                                                    <Col xs={2} md={2} >
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>

                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>
                                                    <Col xs={2} md={2}>
                                                        <Card>
                                                            <CardImg
                                                                src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                                alt=""/>

                                                            <Card.Footer>
                                                                <small className="text-muted">Last updated 3 mins
                                                                    ago</small>
                                                            </Card.Footer>
                                                        </Card>
                                                    </Col>



                                                </Row>
                                            </Container>

                                        </div>

                                    </Col>
                                </Row>


                            </Card>

                    </Card.Text>

                </Card.Body>
                <Card.Footer className="text-muted"> <Footer/> </Card.Footer>
            </Card>


        </div>
    }

}



