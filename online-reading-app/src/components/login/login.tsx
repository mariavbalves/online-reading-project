import React from 'react';
import { Col, Container, Row, Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../login/login.css'
import People from '../../people/people'

import 'bootstrap/dist/css/bootstrap.min.css';
// @ts-ignore
import {BrowserRouter, Route,Link, Switch} from 'react-router-dom';
import Onread from '../Onreadtag/Onread';



export default class Login extends React.Component{
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    
  }
  state={
    email:"",
    password:"",
  }

  handleChange = (e: { target: { name: any; value: any; }; }) =>{
    const {name,value}=e.target
    this.setState({[name]:value})
  }

  handleSubmit = (e: { preventDefault: () => void; }) =>{
    e.preventDefault();
  }
  render() {
        return <Container fluid className="centralize">
        <Row className="section">
        <Onread/>
        </Row>
        <Row className="section">
            <Form onSubmit={this.handleSubmit}> 
            <Form.Group className="mb-3" controlId="formBasicEmail">
              
              <Form.Control className="formLabelLogin"  style={{color:"#5399cf"}}  type="email" placeholder="Enter email" required onChange={this.handleChange}/>
              <Form.Text style={{color:"#003d6b"}} className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              
              <Form.Control  className="formLabelLogin" style={{color:"#5399cf"}} type="password" placeholder="Password" required onChange={this.handleChange}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
              <Form.Check style={{color:"#003d6b"}} type="checkbox" label="Check me out" />
            </Form.Group>
                <p>Aren't you registered yet? <Link to="/Register">Register here.</Link></p>

            <Link to="/mainPage"><Button  onSubmit={this.handleSubmit} variant="outline-primary" className="buttonLogin" type="submit">Submit</Button></Link>


        </Form>
        
        </Row>
      </Container>
           



       

}
}




