import React from 'react';
// @ts-ignore
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import NavBar from "../navBar";

import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Button,
    Card,
    CardGroup,
    CardImg,
    Col,
    Container,
    Form,
    FormControl,
    Image,
    Nav,
    Navbar,
    Row
} from "react-bootstrap";
import Footer from "../footer";


export default class Article extends React.Component {
    render() {

        return <div>


            <Card >


                <NavBar/>

                <Card.Header>
                </Card.Header>


                <Card.Body>

                    <Card.Text>



                        <Container className="text-center">
                            <h1>Why is reading so good? </h1>


                            <Image src="https://www.collinsdictionary.com/images/full/reading_339775121_1000.jpg"
                                   width={"500px"} height={"200px"} thumbnail/>

                            <p>Published on 19th October 2021</p>

                        </Container>

                            <Container  style={{width: '50rem'}} className="justify-content-lg-center ">

                            <p>When was the last time you read a book, or a substantial magazine article? Do your daily reading habits center around tweets, Facebook updates, or the directions on your instant oatmeal packet?

                                If you’re one of countless people who don’t make a habit of reading regularly, you might be missing out.

                                Reading has a significant number of benefits, and here’re 5 benefits of reading to get you to start reading.</p>

                                <p> <h4> 1. Mental Stimulation </h4>
                                    Studies have shown that staying mentally stimulated can slow the progress of (or possibly even prevent) Alzheimer’s and Dementia,[1] since keeping your brain active and engaged prevents it from losing power.
                                    Just like any other muscle in the body, the brain requires exercise to keep it strong and healthy, so the phrase “use it or lose it” is particularly apt when it comes to your mind. Doing puzzles and playing games such as chess have also been found to be helpful with cognitive stimulation.
                                </p>
                                <p>
                                   <h4> 2. Stress Reduction </h4>
                                    No matter how much stress you have at work, in your personal relationships, or countless other issues faced in daily life, it all just slips away when you lose yourself in a great story. A well-written novel can transport you to other realms, while an engaging article will distract you and keep you in the present moment, letting tensions drain away and allowing you to relax.</p>

                                <p> <h4> 3. Knowledge </h4>
                                    Everything you read fills your head with new bits of information, and you never know when it might come in handy. The more knowledge you have, the better-equipped you are to tackle any challenge you’ll ever face.

                                    Additionally, here’s a bit of food for thought: should you ever find yourself in dire circumstances, remember that although you might lose everything else—your job, your possessions, your money, even your health—knowledge can never be taken from you.
                                </p>

                                <p> <h4> 4. Vocabulary Expansion </h4>
                                    The more you read, the more words you gain exposure to, and they’ll inevitably make their way into your everyday vocabulary.

                                    Being articulate and well-spoken is of great help in any profession, and knowing that you can speak to higher-ups with self-confidence can be an enormous boost to your self-esteem. It could even aid in your career, as those who are well-read, well-spoken, and knowledgeable on a variety of topics tend to get promotions more quickly (and more often) than those with smaller vocabularies and lack of awareness of literature, scientific breakthroughs, and global events.
                                </p>
                                <p> <h4> 5. Memory Improvement </h4>
                                    When you read a book, you have to remember an assortment of characters, their backgrounds, ambitions, history, and nuances, as well as the various arcs and sub-plots that weave their way through every story. That’s a fair bit to remember, but brains are marvellous things and can remember these things with relative ease.

                                    Amazingly enough, every new memory you create forges new synapses (brain pathways)[3] and strengthens existing ones, which assists in short-term memory recall as well as stabilizing moods.[4] How cool is that?                                </p>

                            </Container>


                        <br/>
                        <br/>
                        <Container className="text-center">
                            <h2>The Creator </h2>
                            <p>The creators of the article.</p>
                            <Container>
                                <Row>

                                    <Col xs={12} md={12}>
                                        <Image
                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                            width={"200px"} height={"200px"} roundedCircle/>
                                        <h2>user7374647</h2>
                                        <p>My name is Francisco Sousa. I really like to read, so I decided to create this article. I hope you enjoy it.</p>

                                        <br/>
                                        <br/>
                                    </Col>

                                </Row>

                            </Container>
                        </Container>





                    </Card.Text>

                </Card.Body>
                <Card.Footer className="text-muted"> <Footer/> </Card.Footer>
            </Card>


        </div>
    }

}



