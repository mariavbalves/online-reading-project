import React from 'react';
// @ts-ignore
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';


import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Button,
    Card,
    CardGroup,
    CardImg,
    Col,
    Container,
    Form,
    FormControl,
    Image,
    Nav,
    Navbar,
    Row
} from "react-bootstrap";
import Footer from "../footer";
import NavBar from "../navBar";


export default class BooksOnRead extends React.Component {
    render() {
        return <div>
            <Card className="text-center">

                <NavBar/>

                <Card.Header>
                </Card.Header>


                <Card.Body>

                    <Card.Text>
                        <Container>
                            <Card bg="light" style={{width: '100%'}}>
                                <Link to='/userData'>
                                    <div>Back to your user profile</div>
                                </Link>
                                <Container>

                                    <Image
                                        src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                        width={"200px"} height={"200px"} roundedCircle/>
                                    <h2>Maria Alves </h2>


                                    <h3>25 years old - Barcelos</h3>
                                </Container>
                                <p> Books on-read </p>

                                <Container>


                                    <CardGroup>

                                        <Container>
                                            <Row>

                                                <Col xs={6} md={4}>
                                                    <Card>
                                                        <CardImg
                                                            src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                            alt=""/>

                                                        <Card.Body>
                                                            <Card.Title>Old man at the sea </Card.Title>
                                                            <Card.Text>
                                                                <p>by Ernest Hemingway </p>
                                                                This short novel, already a modern classic, is the
                                                                superbly told, tragic story of a Cuban fisherman in the
                                                                Gulf Stream and the giant Marlin he kills and
                                                                loses—specifically referred to in the citation
                                                                accompanying the author's Nobel Prize for literature in
                                                                1954.
                                                            </Card.Text>
                                                        </Card.Body>
                                                        <Card.Footer>
                                                            <small className="text-muted">Last updated 3 mins
                                                                ago</small>
                                                        </Card.Footer>
                                                    </Card>

                                                </Col>
                                                <Col xs={6} md={4}>
                                                    <Card>
                                                        <CardImg
                                                            src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                            alt=""/>

                                                        <Card.Body>
                                                            <Card.Title>Old man at the sea </Card.Title>
                                                            <Card.Text>
                                                                <p>by Ernest Hemingway </p>
                                                                This short novel, already a modern classic, is the
                                                                superbly told, tragic story of a Cuban fisherman in the
                                                                Gulf Stream and the giant Marlin he kills and
                                                                loses—specifically referred to in the citation
                                                                accompanying the author's Nobel Prize for literature in
                                                                1954.
                                                            </Card.Text>
                                                        </Card.Body>
                                                        <Card.Footer>
                                                            <small className="text-muted">Last updated 3 mins
                                                                ago</small>
                                                        </Card.Footer>
                                                    </Card>
                                                </Col>
                                                <Col xs={6} md={4}>

                                                    <Card>
                                                        <CardImg
                                                            src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                            alt=""/>

                                                        <Card.Body>
                                                            <Card.Title>Old man at the sea </Card.Title>
                                                            <Card.Text>
                                                                <p>by Ernest Hemingway </p>
                                                                This short novel, already a modern classic, is the
                                                                superbly told, tragic story of a Cuban fisherman in the
                                                                Gulf Stream and the giant Marlin he kills and
                                                                loses—specifically referred to in the citation
                                                                accompanying the author's Nobel Prize for literature in
                                                                1954.
                                                            </Card.Text>
                                                        </Card.Body>
                                                        <Card.Footer>
                                                            <small className="text-muted">Last updated 3 mins
                                                                ago</small>
                                                        </Card.Footer>
                                                    </Card>
                                                </Col>
                                                <Col xs={6} md={4}>

                                                    <Card>
                                                        <CardImg
                                                            src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                            alt=""/>

                                                        <Card.Body>
                                                            <Card.Title>Old man at the sea </Card.Title>
                                                            <Card.Text>
                                                                <p>by Ernest Hemingway </p>
                                                                This short novel, already a modern classic, is the
                                                                superbly told, tragic story of a Cuban fisherman in the
                                                                Gulf Stream and the giant Marlin he kills and
                                                                loses—specifically referred to in the citation
                                                                accompanying the author's Nobel Prize for literature in
                                                                1954.
                                                            </Card.Text>
                                                        </Card.Body>
                                                        <Card.Footer>
                                                            <small className="text-muted">Last updated 3 mins
                                                                ago</small>
                                                        </Card.Footer>
                                                    </Card>
                                                </Col>
                                                <Col xs={6} md={4}>

                                                    <Card>
                                                        <CardImg
                                                            src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                            alt=""/>

                                                        <Card.Body>
                                                            <Card.Title>Old man at the sea </Card.Title>
                                                            <Card.Text>
                                                                <p>by Ernest Hemingway </p>
                                                                This short novel, already a modern classic, is the
                                                                superbly told, tragic story of a Cuban fisherman in the
                                                                Gulf Stream and the giant Marlin he kills and
                                                                loses—specifically referred to in the citation
                                                                accompanying the author's Nobel Prize for literature in
                                                                1954.
                                                            </Card.Text>
                                                        </Card.Body>
                                                        <Card.Footer>
                                                            <small className="text-muted">Last updated 3 mins
                                                                ago</small>
                                                        </Card.Footer>
                                                    </Card>
                                                </Col>
                                                <Col xs={6} md={4}>

                                                    <Card>
                                                        <CardImg
                                                            src="https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1630572642i/2165.jpg"
                                                            alt=""/>

                                                        <Card.Body>
                                                            <Card.Title>Old man at the sea </Card.Title>
                                                            <Card.Text>
                                                                <p>by Ernest Hemingway </p>
                                                                This short novel, already a modern classic, is the
                                                                superbly told, tragic story of a Cuban fisherman in the
                                                                Gulf Stream and the giant Marlin he kills and
                                                                loses—specifically referred to in the citation
                                                                accompanying the author's Nobel Prize for literature in
                                                                1954.
                                                            </Card.Text>
                                                        </Card.Body>
                                                        <Card.Footer>
                                                            <small className="text-muted">Last updated 3 mins
                                                                ago</small>
                                                        </Card.Footer>
                                                    </Card>
                                                </Col>
                                            </Row>

                                        </Container>

                                    </CardGroup>
                                </Container>


                            </Card>
                        </Container>


                    </Card.Text>

                </Card.Body>
                <Card.Footer className="text-muted">  <Footer/> </Card.Footer>
            </Card>


        </div>
    }
}
