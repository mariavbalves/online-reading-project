import React from 'react';
// @ts-ignore
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Button,
    Card,
    CardGroup,
    CardImg,
    Col,
    Container,
    Form,
    FormControl,
    Image,
    Nav,
    Navbar,
    Row
} from "react-bootstrap";
import Footer from "../footer";
import NavBar from "../navBar";


export default class Help extends React.Component {
    render() {

        return <div>


            <Card>


                <NavBar/>

                <Card.Header>
                </Card.Header>


                <Card.Body>

                    <Card.Text>


                        <Container className="text-center">
                            <h1>Help </h1>

                            <h2>Frequently Asked Questions (FAQ): </h2>
                        </Container>

                        <Container  style={{width: '65rem'}} className="justify-content-lg-center ">

                            <br/>
                            <br/>
                            <p> <h4> What is it for the  <Link to="./Booksread"> Books Read page?</Link></h4>

                                This page is to store and show the books you already read.
                            </p>

                            <p> <h4> What is it for the  <Link to="./BooksOnread"> Books on-read page?</Link></h4>

                                This page is to store and show the books you are currently reading. We hope a lot :)
                            </p>
                            <p> <h4> What is it for the  <Link to="./ToreadBooks"> To-read Books page?</Link></h4>

                                This page is to store and show your "read-list" - The books you want to read in the future.</p>
                            <p> <h4> What is it for the  <Link to="./Favorites"> Favorites page?</Link></h4>

                                This page is to store and show your most liked books of all time.
                            </p>

                            <br/>
                            <p> <h4> Are all authors in the world on the <Link to="./Booksread"> Authors page?</Link></h4>

                                No. It's only a suggestion by us to check it out!
                            </p>


                        </Container>

                    </Card.Text>

                </Card.Body>
                <Card.Footer className="text-muted">
                    <Footer/>


                </Card.Footer>
            </Card>


        </div>
    }

}

