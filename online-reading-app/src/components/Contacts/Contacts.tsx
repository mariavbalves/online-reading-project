import React from 'react';
// @ts-ignore
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import NavBar from "../navBar";

import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Button,
    Card,
    CardGroup,
    CardImg,
    Col,
    Container,
    Form,
    FormControl,
    Image,
    Nav,
    Navbar,
    Row
} from "react-bootstrap";
import Footer from "../footer";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebook, faInstagram, faTwitter, faYoutube} from "@fortawesome/free-brands-svg-icons";


export default class Contacts extends React.Component {
    render() {

        return <div>


            <Card>


                <NavBar/>

                <Card.Header>
                </Card.Header>


                <Card.Body>

                    <Card.Text>


                        <Container>
                            <h1 className='text-center'>Contacts </h1>

                            <Card bg="light" style={{width: '100%'}} className='justify-content-lg-center'>


                                <Form className="d-flex">

                                    <FormControl
                                        type="search"
                                        placeholder="Search"
                                        className="mr-2"
                                        aria-label="Search"
                                        style={{width: '20rem', padding: '5px', margin: '20px'}}
                                    />

                                    <Button variant="light">Search</Button>

                                </Form>


                                <Row>
                                    <Col xs={6} md={4}>

                                        <div>

                                            <Container style={{width: '85rem', margin: '100px', marginTop: '20px'}}
                                                       className="justify-content-lg-center ">
                                                <Row>
                                                    <Col xs={6} md={4} >
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p ><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Afonso
                                                            Pedroso </h4></p>

                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>
                                                            <h4>Oriana </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>

                                                        </div>
                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>
                                                            <h4>Andrei Cuandra </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Ana
                                                            Margarida Costa </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Igor
                                                            Ponte</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Telmo
                                                            Ponte </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Ricardo
                                                            Araujo </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >

                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>João
                                                            Cardoso </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >

                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>
                                                            <h4>Larissa Oliveira</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col> <Col xs={6} md={4}>
                                                    <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >

                                                    <p><Image
                                                        src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                        width={"50px"} height={"50"} roundedCircle/>  <h4>Mariana
                                                        Jesus</h4>
                                                    </p>


                                                    <Link to="./userProfile">
                                                        <Image
                                                            src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                            width={"30px"} height={"30px"} rounded/></Link>
                                                    </div>

                                                </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >

                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>
                                                            <h4>Johnny Moura </h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>

                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                         <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >

                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Luís
                                                            Pinto</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                         </div>
                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Yolanda
                                                            Sampaio</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>
                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Filipe
                                                            Lopes</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>
                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Pedro
                                                            Rodrigues</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>
                                                    </Col>
                                                    <Col xs={6} md={4}>
                                                        <div style={{background: "white", width: '15rem', margin: '5px', borderStyle: 'solid', borderRadius:'25px', padding:'10px'}} >
                                                        <p><Image
                                                            src="https://icons.veryicon.com/png/o/miscellaneous/two-color-webpage-small-icon/user-244.png"
                                                            width={"50px"} height={"50"} roundedCircle/>  <h4>Rafael
                                                            Costa</h4></p>


                                                        <Link to="./userProfile">
                                                            <Image
                                                                src="https://images.vexels.com/media/users/3/136340/isolated/preview/74ac661d8216442ae469efb39c9584dc-mail-message-icon.png"
                                                                width={"30px"} height={"30px"} rounded/></Link>
                                                        </div>
                                                    </Col>


                                                </Row>
                                            </Container>
                                        </div>
                                    </Col>
                                </Row>


                            </Card>
                        </Container>


                    </Card.Text>

                </Card.Body>
                <Card.Footer className="text-muted"> <Footer/> </Card.Footer>
            </Card>


        </div>
    }

}



