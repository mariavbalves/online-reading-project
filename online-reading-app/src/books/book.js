books=[{
    id:1,
    name:"the boy with the stripped pijama",
    genre: "drama",
},
{
    id:2,
    name:"Think and grow rich",
    genre: "self-development",
},
{
    id:3,
    name:"HarryPotter and the philosophal stone",
    genre: "J.K Rowling",
},
{
    id:4,
    name:"Rich Dad poor Dad",
    genre: " Robert Kiyosaki and Sharon L. Lechter",
},
{
    id:5,
    name:"Os Maias",
    genre: "Eça de Queiroz",
},
]; export default books;